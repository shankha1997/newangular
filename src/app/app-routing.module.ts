import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomePageComponent } from "./home-page/home-page.component";
import { ProductDetailsPageComponent } from "./product-details-page/product-details-page.component";
import { CartPageComponent } from "./cart-page/cart-page.component";

const routes: Routes = [
  {
    component: HomePageComponent,
    path:''
  },
  {
    component: ProductDetailsPageComponent,
    path: 'details'
  },
  {
    component: CartPageComponent,
    path: 'cart'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
